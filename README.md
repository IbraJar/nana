# Support-L4-assignment

## Introduction

Gaining the knowledge necessary to pass to this level requires having excellent solving skills for first-line tickets, very good knowledge of PS products with minimal support from the upper level, and good knowledge of DevOps technologies. 

## Skill sets will be measured in this assignment:
1- Linux / Bash scripting <br />
2- Monitoring and tuning <br />
3- DevOps technologies <br />
4- Database / DBA <br />
5- Application Servers <br />
6- PS Products (Business and Technical) <br />


## You can refer to the below official sites to help you through this assignment:
1- [Linux](link)<br />
2- [Monitoring](link)<br />
3- [DevOps](link)<br />
4- [Database](link)<br />
5- [DBA](link)<br />
6- [Application Servers](link)<br />
7- [PS Products - STC ](link)<br />
8- [PS Products - mPAY ](link)<br />

## [Go to assignments](Assignments/TakeAssignment.md)