### linux:

  1- write script to view date as below format with same order: 
    - Note : use current date inside scripts not below date.

  ```
  Fri Aug 30 13:05:09 EST 2019
  08-30-19
  08-30-2019
  13:05:09
  01:05:09 PM
  13:05
  Friday 30 Aug 2019 13:05:09
  Nanoseconds: 1567159562-373152585
  Timezone: 06:05:09
  Timezone: 10:05:09
  1567159509
  Week number: 35
  -rw-r--r--  1 mtsouk  staff  0 Aug 30 13:05 1567159509
  -rw-r--r--  1 mtsouk  wheel  0 Aug 30 13:05 /tmp/test.1567159509
  ```

 2- write linux function to have one param  Threshold and print out all disks which have less than total available space of this Threshold param.

  ```
  You set Threshold = 20
  WARNING: The partition "/dev/root" has used 10% of total available space - Date: Wed Sep 20 22:14:11 EEST 2022
  ```

 3- write script will look into a predefined list of directories and count the number of files that exist in each directory and its subdirectories. If that number is above a threshold, then the script will generate a warning message.

  Example output must be like below : 


  ```
  WARNING: Large number of files in /bin: 200!
  Everything is fine in /home/awad/code: 80
  WARNING: Large number of files in /u01/logs: 350!
  ```

### scripts :

  - Write script to keep monitor one of PS product and in case and outage of service to send and email to you.
  - The script must keep try to check the site for around 5 minute in case the site is un healthy to restat the service.
   
### Networking:

 - use tcpdump tool for one PS product.
 - install wireshark to monitor one PS product.

### Monitor tools:

  - install prometheus and monitor one PS product ( DB and APP).

## MSSQL :

   - Insall microsoft sql server 2019 on VM
   - run microsoft sql server 2019 as docker on LINUX.
   - Create database with username & password for this database.
   - Create sample tables then export this database as dump.
   - Import this dump on new database.

### Devops(containers):

  using docker compose run one of PS product.


## SQL:


### hints: 

 You can use oracle docker image to practice this assignment : 

 * note that you may connect to progressoft docker hub  using below caommand 
 ```
   docker login -u psdeveloper -p 00fa32a8-ce58-4f8d-9f72-42956b3999d7

 ```

 * You can find last password from below link : 

 ```
   https://gitlab.com/progressoft/devops/genesis/k8s-configs
 ```

 ```
  sudo mkidr -p /opt/oracle/oradata/ 
  sudo chmod -R 777 /opt/oracle
  sudo chown -R $USER:$USER /opt/oracle

  docker run -d -p 1521:1521 -v /opt/oracle/oradata/:/opt/oracle/oradata/ progressoft/oracle:19.3.0-ee
 ```



### Practice

Q1.Create Oracle user
 
  The user must be : 
  ```
     Username    : #YOURNAME#
     Password    : #YOURNAME#
     role Access : DBA. 
  ```

* Note: For below Assignment , create all below packages , procedures , tables , and tables under #YOURNAME#  schema.

Q2.SQL/PLSQL

Create  below PL / SQL package:  Package Name : #YOUNAME# 

Procedure 1: P_PRINT_HELLO  , write procedure  to print  : Hello , World ! #YOURNAME# 

Procedure 2: P_CREATE_TAB_EMP : write PL/SQL procedure to create below table : 

EMP 
```
Name          Type 
EMPNO         NUMBER(4) 
ENAME         VARCHAR2(10) 
JOB           VARCHAR2(9) 
MGR           NUMBER(4) 
HIREDATE      DATE 
SAL           NUMBER(7,2) 
COMM          NUMBER(7,2) 
DEPTNO         NUMBER(2) 
```
   
Procedure 3: P_CREATE_TAB_DEPT : write PL/SQL procedure to create below table : 
DEPT : 
```
Name         Type 
DEPTNO      NUMBER(2) 
DNAME       VARCHAR2(14) 
LOC         VARCHAR2(13) 
```

* While Creating the above tables:  

```
Define the primary keys on tables 
Define unique keys 
Define foreign keys between tables 
Define check constraints on tables. 

```

  
Procedure 4: P_INSERT_DATA : write PL/SQL procedure to  insert   data on above tables. 
EMP :  

```
EMPNO        ENAME          JOB            MGR          HIREDATE          SAL        COMM         DEPTNO 
7369         SMITH          CLERK          7902         17-DEC-80         800                      20 
7499         ALLEN          SALESMAN       7698         20-FEB-81         1600       300           30 
7521         WARD           SALESMAN       7698         22-FEB-81         1250       500           30 
7566         JONES          MANAGER        7839         02-APR-81         2975                     20 
7654         MARTIN         SALESMAN       7698         28-SEP-81         1250       1400          30 
7698         BLAKE          MANAGER        7839         01-MAY-81         2850                     30 
7782         CLARK          MANAGER        7839         09-JUN-81         2450                     10 
7788         SCOTT          ANALYST        7566         19-APR-87         3000                     20 
7839         KING           PRESIDENT                   17-NOV-81         5000                     10 
7844         TURNER         SALESMAN       7698         08-SEP-81         1500       0             30 
7876         ADAMS          CLERK          7788         23-MAY-87         1100                     20 
7900         JAMES          CLERK          7698         03-DEC-81         950                      30 
7902         FORD           ANALYST        7566         03-DEC-81         3000                     20 
7934         MILLER         CLERK          7782         23-JAN-82         1300                     10 
```

Dept :  

```
DEPTNO               DNAME           LOC 
10                   ACCOUNTING      NEW YORK 
20                   RESEARCH        DALLAS 
30                   SALES           CHICAGO 
40                   OPERATIONS      BOSTON 
```

Procedure 5:  P_PRINT_EMP_SAL : write procedure to print below statement:  

 ```
   Salary for Employ #EMP_NAME#  from Department  #Dept_Name =  #SAL# !  
 ```
 
Procedure 6:  P_UPDATE_SAL : procedure to send employ number  and new salary to update salary for specific employ . 

Procedure 7:  P_SQL_FUN : Displays the employees’ names and indicates the amounts of their salaries through asterisks. Each asterisk signifies a hundred dollars. Sort the data in descending order of salary.
  
 ``` 
    EMPLOYEE_AND_THEIR_SALARIES 
    ----------------------------------------------------------------------- 
    KING      ************************************************** 
    FORD     ****************************** 
    SCOTT    ****************************** 
 ```



Procedure 8:  P_DELETE_USER : Write procedure to delete DEPT_ID =  20 



Q3.Create a database link to select all department from your colleague's database. 


### DBA :

  - install ORACLE 19c on linux.
  - create DB script to view below : 

     ```
      - DB info : Name , status, Version , Memory Size (SGA+PGA) , DB PORT , ARCHIVE_MODE  ...
      - Tablespace size : Name , Total , % Free ....
      - Log info : Online redolog name , path , size
      - Top 10 query.
      - lsnrctl status.
      - Backup RMAN status.
      - other info for DB.
     ```



    - Which command use to show all Oracle background process with status for each one.

    - Write down your recommendation for below parameter

        
        SGA
        PGA
        Number of procress
        Number of Sessions
        
        
        if you have below server spec: 

        
        DB Version = 19c
        OS Type= Linux
        Memory =64G
        Number of CPUs=4
        Number of Connection expected =800 
        Data Storage =SSD
        


  - Create cron job to monitor DB , in Case any error appear start with ORA- to send by email ( the job must run every 15 minute).






## partitioning Practice: 


- Create below table sales ( with range partitions on date column) that have below column with data type : id int, name text, amount int, sale_date date 
- Create range partitions for above table with name sales_2020 for all data for year 2021.
- Create range partitions for above table with name sales_2021_Q1 for all data for year 2021 from peroid (1/1/2022 to 1/4/2022).
- Create range partitions for above table with name sales_2021_Q2 for all data for year 2021 from peroid (1/4/2022 to 31/12/2022).
- Insert sample data into sales table and view data from both partitions and from table itself.


- Create below table region (with LIST partitions on region column) that have below column with data type : (id int, amount int, branch text, region text)
- Add 3 partitions table as a list for data : (London, Sydney, and Boston).

- How to check if table have partitions or not ? 
- Write down query to view all partitions tables.

 # vulnerability :

  Below is report security issue reported from bank , feedback how to reslove them : 

    
    Password time validiation for 30 days
    all unlocked accounts must have a password'
    minimum length of a password is 8
    password re use cycle is 32 .
    Password complexcity at least one capital letter ,one number and one special character
    Password cannot contain the user name
    system accounts are non-login"
    Password similarity New password cannot share more than three characters in the same position as the old password
    Store passwords using reversible encryption is set to 'Disabled'
    password fields are not empty
   

###  tomcat

  - Install Tomcat on 2 VM server.
  - deploy any PS product.
  - use any tool to make load balancer between 2 VM using : HAproxy or Nginx or any other tools
  - the connection must be sticky session type not round-robin. 

### Weblogic 

  
    - manage Node manager
    - try to enable SSL on weblogic and open it using https
    - run docker for weblogic
    - create automated script to make deployment  ( upload war + create datasource + start service then start).
 
### NOSQL

  - Install mongoDB
  - create database with Your name.
  - create Collections with name Collections_YOURNAME.
  - create document on this DB and insert below data :
   
    ```
     firstName : Yourname
     lastName : YourlastName
     email: Youremail
    ```

  - List all data of a collection and format resulted data.
  - Insert Multiple Documents in a Collection on one command (create dummy data)
  - Delete Documents in a Collection 


### Migration scripts 

  - create first schema with name schema01 on oracle DB with below scripts :

  ```
   create table dept(
        deptno   number(2,0) not null,
        dname    varchar2(14),
        loc      varchar2(13));

 create table emp(
        empno    number(4,0) not null,
        ename    varchar2(10),
        job      varchar2(9),
        mgr      number(4,0),
        hiredate date,
        sal      number(7,2),
        comm     number(7,2),  
        deptno   number(2,0) not null);

create table bonus(
        ename    varchar2(10),
        job      varchar2(9),
        sal      number,
        comm     number);

create table salgrade(
        grade    number,
        losal    number,
        hisal    number);

create table dummy (
        dummy    number);

      insert into dummy values (0);
      insert into DEPT (DEPTNO, DNAME, LOC)
        select 10, 'ACCOUNTING', 'NEW YORK' from dummy union all
        select 20, 'RESEARCH',   'DALLAS'   from dummy union all
        select 30, 'SALES',      'CHICAGO'  from dummy union all
        select 40, 'OPERATIONS', 'BOSTON'   from dummy;
      insert into emp (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO)
        select 7839, 'KING',   'PRESIDENT', null, to_date('17-11-1981','dd-mm-yyyy'),    5000, null, 10 from dummy union all
        select 7698, 'BLAKE',  'MANAGER',   7839, to_date('1-5-1981','dd-mm-yyyy'),      2850, null, 30 from dummy union all
        select 7782, 'CLARK',  'MANAGER',   7839, to_date('9-6-1981','dd-mm-yyyy'),      2450, null, 10 from dummy union all
        select 7566, 'JONES',  'MANAGER',   7839, to_date('2-4-1981','dd-mm-yyyy'),      2975, null, 20 from dummy union all
        select 7788, 'SCOTT',  'ANALYST',   7566, to_date('13-JUL-87','dd-mm-rr') - 85,  3000, null, 20 from dummy union all
        select 7902, 'FORD',   'ANALYST',   7566, to_date('3-12-1981','dd-mm-yyyy'),     3000, null, 20 from dummy union all
        select 7369, 'SMITH',  'CLERK',     7902, to_date('17-12-1980','dd-mm-yyyy'),     800, null, 20 from dummy union all
        select 7499, 'ALLEN',  'SALESMAN',  7698, to_date('20-2-1981','dd-mm-yyyy'),     1600,  300, 30 from dummy union all
        select 7521, 'WARD',   'SALESMAN',  7698, to_date('22-2-1981','dd-mm-yyyy'),     1250,  500, 30 from dummy union all
        select 7654, 'MARTIN', 'SALESMAN',  7698, to_date('28-9-1981','dd-mm-yyyy'),     1250, 1400, 30 from dummy union all
        select 7844, 'TURNER', 'SALESMAN',  7698, to_date('8-9-1981','dd-mm-yyyy'),      1500,    0, 30 from dummy union all
        select 7876, 'ADAMS',  'CLERK',     7788, to_date('13-JUL-87', 'dd-mm-rr') - 51, 1100, null, 20 from dummy union all
        select 7900, 'JAMES',  'CLERK',     7698, to_date('3-12-1981','dd-mm-yyyy'),      950, null, 30 from dummy union all
        select 7934, 'MILLER', 'CLERK',     7782, to_date('23-1-1982','dd-mm-yyyy'),     1300, null, 10 from dummy;
      insert into salgrade
        select 1,  700, 1200 from dummy union all
        select 2, 1201, 1400 from dummy union all
        select 3, 1401, 2000 from dummy union all
        select 4, 2001, 3000 from dummy union all
        select 5, 3001, 9999 from dummy;
      commit;
 ```

 - create schema2 with below table : 


 ```
    create table dept(
        deptno   number(2,0) not null,
        dname    varchar2(14),
        loc      varchar2(13),
        created_date date,
        created_by varchar2(20),
        employee_pic blob);

      create table emp(
        empno    number(4,0) not null,
        ename    varchar2(10) not null,
        last_name  varchar2(10) not null, 
        job      varchar2(9),
        mgr      number(4,0),
        hiredate date,
        sal      number(7,2),
        deptno   number(2,0) not null);

      create table bonus(
        ename    varchar2(10),
        job      varchar2(9),
        sal      number);
      create table salgrade(
        grade    number,
        losal    number,
        hisal    number);

      create table dummy (
        dummy    number,
        note     clob);

 ```

 - write script to copy all data from all tables which exist on schema1 to schema2 and you need to copy all constraitns which on schema1.

### Product assgiment :

  - ## [STC Assignment ](PS-Products/STC/TakeAssignment.md)
  - ## [Mobile Assignment ](PS-Products/mPAY/TakeAssignment.md)
  - ## [ECC Assignment ](PS-Products/ECC/TakeAssignment.md)


### jprofiler :

 - install jprofiler on your pc or VM
 - configure jprofiler to monitor tomcat application. 