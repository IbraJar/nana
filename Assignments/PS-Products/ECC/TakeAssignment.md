
### Deplyment : 


#### ECC deployment Assignment  
 
  1. After watching JOPACC deployment video you have requested to deploy ECC with new scan 
  page on tomcat. 
  
  releases URL: 
  https://gitlab.com/progressoft/checks/ecc-jordan/jopacc/jopacc-main/-/releases 
  https://gitlab.com/progressoft/checks/ecc-jordan/jopacc/jopacc-new-scan/-/releases 
  
  2. Deploy commercial and central side.  
  
  (Hint: Use the same above artifacts) 
  
  3. Deploy the latest services  
  
  Services release URL: 
  https://gitlab.com/progressoft/checks/ecc-jordan/jopacc/jopacc-services/-/releases 
  
  4. Do a Smoke test by sending cheques from ECC to the commercial bank.  
  
  
  5. Create a bash script that starts and stops the services without running them manually 
  (Extra Question). 
  
  
  Scan Page manual readme file: 
  https://gitlab.com/progressoft/checks/sun/unified-micro-services/back-
  end/ecc/core#starting-from-v400-and-higher 