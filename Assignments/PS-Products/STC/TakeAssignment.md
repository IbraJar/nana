## STC Business assignment

Explain in details what is SOA?




What is Appdynamics, and what are some uses for it?




Clearly describe SMTP?



Explain in details what is KNET?



Describe in full the STC system's workflow for each request, taking into consideration the involvement of a third party for payment validation, from the point at which a customer makes a transaction to the point at which it is saved in the database.




Describe how to determine which STC channels are available for use and which are not.



Define each channel and to which party it follows.



Briefly describe each of the STC channels.



Give specific instructions on how to export a report that shows fulfillment rejections and failures.
Describe in detail how to export a Kiosk report.



Briefly describe the STC transaction limits and how they operate.



Define the following attributes for STC security: Realm, Client, Roles, Authentication, Groups, Users, Sections, and Event.




Give precise instructions on how to use the two techniques below to track down a particular order ID (UI, Database).



How can we determine whether a fulfillment refused status on an order refers to a business or technical error?

## STC Technical assignment